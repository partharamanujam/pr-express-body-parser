"use strict";

var bodyParser = require("body-parser"),
    connectBusboy = require("connect-busboy");

var defaults = {
    "json": {
        "limit": "1mb",
        "type": ["json", "*/json", "+json"]
    },
    "text": {
        "limit": "1mb",
        "type": ["text/*"]
    },
    "urlencoded": {
        "limit": "1mb",
        "extended": true
    },
    "raw": {
        "limit": "1mb",
        "type": "application/*"
    },
    "multipart": {} // use busboy defaults
};

function getOptions(options) {
    var opts = options || {};

    opts = Object.assign.apply(null, Object.keys(defaults).map(prop => ({
        [prop]: opts[prop]
    })));
    Object.keys(defaults).forEach(function each(o) {
        opts[o] = opts[o] || defaults[o];
    });
    opts.multipart.immediate = opts.multipart.hasOwnProperty("immediate") ?
        opts.multipart.immediate : true; // enforce if not set

    return opts;
}

function bodyParsingMiddleware(options) {
    var opts = getOptions(options);

    return [
        bodyParser.json(opts.json),
        bodyParser.text(opts.text),
        bodyParser.urlencoded(opts.urlencoded),
        bodyParser.raw(opts.raw),
        connectBusboy(opts.multipart)
    ];
}

// exports
module.exports = bodyParsingMiddleware;