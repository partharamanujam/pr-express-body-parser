"use strict";

var http = require("http"),
    express = require("express"),
    bodyParsingMiddleware = require("../index");


var app, server;

// setup express
app = express();
app.use(bodyParsingMiddleware());
app.post("/body",
    function postVaue(req, res) {
        res.json({
            "type": req.get("Content-Type"),
            "value": req.body
        });
    }
);
app.post("/raw",
    function postVaue(req, res) {
        res.json({
            "type": req.get("Content-Type"),
            "value": new Buffer(req.body).toString()
        });
    }
);
app.post("/multipart",
    function postVaue(req, res) {
        var data = {};

        req.busboy.on("field", function onField(fieldname, val) {
            data[fieldname] = val;
        });
        req.busboy.on("finish", function onFinish() {
            res.json({
                "type": "multipart/form-data",
                "value": data
            });
        });
    }
);

// setup http-server
server = http.createServer(app);

// export
module.exports = server;