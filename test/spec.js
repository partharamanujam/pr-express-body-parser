"use strict";

var server = require("./server"), // eslint-disable-line no-mixed-requires
    supertest = require("supertest"),
    request = supertest.agent(server);

/* globals describe, beforeEach, afterEach, it */
describe("testing express-body-parser", function testExpressBodyParser() {
    beforeEach(function before() {
        server.listen(9090); // eslint-disable-line no-magic-numbers
    });
    afterEach(function after(done) {
        server.close(done);
    });
    it("JSON body", function getValue(done) {
        request
            .post("/body")
            .set("Content-Type", "application/json")
            .send('{"hello": "world"}')
            .expect(200, { // eslint-disable-line no-magic-numbers
                "type": "application/json",
                "value": {
                    "hello": "world"
                }
            }, done);
    });
    it("TEXT body", function getValue(done) {
        request
            .post("/body")
            .set("Content-Type", "text/plain")
            .send("hello world")
            .expect(200, { // eslint-disable-line no-magic-numbers
                "type": "text/plain",
                "value": "hello world"
            }, done);
    });
    it("URL-encoded body", function getValue(done) {
        request
            .post("/body")
            .send("key1=hello")
            .send("key2=world")
            .expect(200, { // eslint-disable-line no-magic-numbers
                "type": "application/x-www-form-urlencoded",
                "value": {
                    "key1": "hello",
                    "key2": "world"
                }
            }, done);
    });
    it("RAW body", function getValue(done) {
        request
            .post("/raw")
            .set("Content-Type", "application/octet-stream")
            .send(new Buffer("hello world"))
            .expect(200, { // eslint-disable-line no-magic-numbers
                "type": "application/octet-stream",
                "value": "hello world"
            }, done);
    });
    it("MULTIPART body", function getValue(done) {
        request
            .post("/multipart")
            .field("field1", "hello")
            .field("field2", "world")
            .expect(200, { // eslint-disable-line no-magic-numbers
                "type": "multipart/form-data",
                "value": {
                    "field1": "hello",
                    "field2": "world"
                }
            }, done);
    });
});