# pr-express-body-parser

Express middleware for parsing different body-types

## Philosophy

Parsing of all different body-types in Express under one roof

## Installation

```bash
$ npm install pr-express-body-parser
```

## APIs

The module exports express-middleware function bodyParsingMiddleware(config); the config is a JSON object with options for different body-types.

The default config-options are:
```js
{
    "json": {
        "limit": "1mb",
        "type": ["json", "*/json", "+json"]
    },
    "text": {
        "limit": "1mb",
        "type": ["text/*"]
    },
    "urlencoded": {
        "limit": "1mb",
        "extended": true
    },
    "raw": {
        "limit": "1mb",
        "type": "application/*"
    },
    "multipart": {} // use busboy defaults
}
```

Refer to the individual module documentation for detailed config-options:
* [json](https://github.com/expressjs/body-parser#bodyparserjsonoptions)
* [text](https://github.com/expressjs/body-parser#bodyparsertextoptions)
* [urlencoded](https://github.com/expressjs/body-parser#bodyparserurlencodedoptions)
* [raw](https://github.com/expressjs/body-parser#bodyparserrawoptions)
* [multipart](https://www.npmjs.com/package/busboy#busboy-methods)

## Usage

```js
var express = require("express").
    expressBodyParser = require("pr-express-body-parser");

var app = express();
express.use(expressBodyParser());
```

For all body-types except multipart, the request-body data is available in req.body property. In case of multiparty data, it is available via req.busboy as [Busboy events](https://www.npmjs.com/package/busboy#busboy-special-events) and related streams.

Refer to test [server.js](https://gitlab.com/partharamanujam/pr-express-body-parser/blob/master/test/server.js) implementation for usage details.

NOTE: In case of multipart, the request-body is piped to req.busboy by default

## Test

```bash
$ npm install # inside pr-express-body-parser
$ npm test
```
